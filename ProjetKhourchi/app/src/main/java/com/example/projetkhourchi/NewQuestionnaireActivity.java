package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NewQuestionnaireActivity extends AppCompatActivity {

    private EditText editerCategory;
    private Button valider, annuler;

    private QCM qcml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newquestionnaire);

        editerCategory = findViewById(R.id.editCategory);
        valider = findViewById(R.id.validerCategory);
        annuler = findViewById(R.id.annulerCategory);

        qcml = getIntent().getSerializableExtra("key", QCM.class);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editerCategory.getText().toString().equals("")){
                    Toast.makeText(NewQuestionnaireActivity.this, "Veuillez entrer une catégorie", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(NewQuestionnaireActivity.this, NewQuestionActivity.class);
                    intent.putExtra("key", qcml);
                    intent.putExtra("category", editerCategory.getText().toString());
                    startActivity(intent);
                    finish();
                }
            }
        });

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent(NewQuestionnaireActivity.this, MainActivity.class));
                finish();
            }
        });
    }

}
