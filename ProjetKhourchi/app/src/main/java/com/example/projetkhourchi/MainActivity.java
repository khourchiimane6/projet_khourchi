package com.example.projetkhourchi;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.activity.result.ActivityResult;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button BtnQst ,DisplayScore ,BtnExit ,ResetScore ,BtnCreateQst;

    private QCM qcm;
    private ActivityResultLauncher<Intent>launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnQst = findViewById(R.id.btnQst);
        BtnCreateQst = findViewById(R.id.btnCreateQst);
        DisplayScore = findViewById(R.id.DisplayScore);
        BtnExit = findViewById(R.id.btnExit);
        ResetScore = findViewById(R.id.btnResetScore);
        qcm = new QCM();
        try {

            qcm.loadQuestionnaire(getApplicationContext(), getResources());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Erreur chargement", Toast.LENGTH_SHORT).show();
        }

        getResultIntent();

        BtnQst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SelectQuestionnaireActivity.class);
                intent.putExtra("keyQCM", qcm);
                launcher.launch(intent);
            }
        });

        BtnCreateQst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("key", qcm);
                startActivity(intent);
            }
        });

        DisplayScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DisplayScoreActivity.class);
                intent.putExtra("keyQCM", qcm);
                launcher.launch(intent);
            }
        });

        BtnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SAVE SCORE HERE
                try {
                    qcm.saveScoreInFile(getApplicationContext());
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Erreur sauvegarde score", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });

        ResetScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qcm.resetScore();
                Toast.makeText(MainActivity.this, "Score réinitialisé", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getResultIntent() {
        launcher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Intent data = result.getData();
                        switch (result.getResultCode()) {
                            case RESULT_OK:
                                assert data != null;
                                qcm = data.getSerializableExtra("keyQCM", QCM.class);
                                break;
                            case RESULT_CANCELED:
                               break;
                        }
                    }
                }
        );

    }

}