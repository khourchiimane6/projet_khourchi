package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private EditText mdp;
    private Button valider, annuler;

    private QCM qcml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mdp = findViewById(R.id.editMdp);
        valider = findViewById(R.id.validerMdp);
        annuler = findViewById(R.id.annulerMdp);
        qcml = getIntent().getSerializableExtra("key", QCM.class);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mdp.getText().toString().equals("MDP")){
                    Intent intent = new Intent(LoginActivity.this, NewQuestionnaireActivity.class);
                    intent.putExtra("key", qcml);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(LoginActivity.this, "Mot de passe incorrect", Toast.LENGTH_SHORT).show();
                    mdp.getText().clear();
                }
            }
        });

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });
    }

}
