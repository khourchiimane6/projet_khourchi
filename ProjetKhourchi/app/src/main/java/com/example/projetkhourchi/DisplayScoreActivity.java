package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DisplayScoreActivity extends AppCompatActivity {
    private Button BtnAnnuler;
    private LinearLayout linearLayout;

    private QCM qcml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        BtnAnnuler = findViewById(R.id.annulerScore);
        linearLayout = findViewById(R.id.linearScore);

        BtnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent(DisplayScoreActivity.this, MainActivity.class));
                finish();
            }
        });

        qcml = getIntent().getSerializableExtra("keyQCM", QCM.class);
        if (qcml != null) {
            qcml.getQuestionnaires().forEach(questionnaire -> {
                TextView txtView = new TextView(this);
                txtView.setText(String.format("La note du questionnaire de catégorie %s est %s/%s", questionnaire.getCategory(), questionnaire.getScore(), questionnaire.size()));
                linearLayout.addView(txtView);
            });
        }
        TextView txtView = new TextView(this);
        txtView.setText(String.format("La note moyenne est %s/20", qcml.calculMoyenneQCM()));
        linearLayout.addView(txtView);

    }
}
