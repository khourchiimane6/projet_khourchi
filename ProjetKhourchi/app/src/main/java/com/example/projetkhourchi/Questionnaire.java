package com.example.projetkhourchi;

import java.io.Serializable;
import java.util.ArrayList;

public class Questionnaire implements Serializable {

    private String category;
    private ArrayList<Question> questions;
    private float score;

    private Boolean finish;

    public Questionnaire(String category, ArrayList<Question> questions) {
        this.category = category;
        this.questions = questions;
        this.score = 0;
        this.finish = false;
    }

    public String getCategory() {
        return category;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public float size() {
        return questions.size();
    }

    public ArrayList<Question> getListQuestions() {
        return questions;
    }


    // CALCUL SCORE
    public void calculScoreQuestionnaire() {
        questions.forEach(question -> score += question.getPoint());
    }

    public Question getQuestion(int questionNumber) {
        // random question
        Question question = questions.get(questionNumber - 1);
        question.setSelected(true);
        return question;
    }
}
