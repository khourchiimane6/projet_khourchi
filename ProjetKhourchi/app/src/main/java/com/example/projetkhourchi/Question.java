package com.example.projetkhourchi;

import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable {

    private String question;
    private ArrayList<String> responses;
    private int TrueResponse;
    private boolean selected;
    private float point;

    public Question(String question, ArrayList<String> responses, int TrueResponse) {
        this.question = question;
        this.responses = responses;
        this.TrueResponse = TrueResponse;
        point = 0;
        selected = false;

    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<String> getResponses() {
        return responses;
    }

    public int getTrueResponse() {
        return TrueResponse;
    }

    public void setSelected(boolean b) {
        this.selected = b;
    }

    public float getPoint() {
        return point;
    }

    public void setPoint(float point) {
        this.point = point;
    }

    public boolean isSelected() {
        return selected;
    }

    public void checkResponseUser(String userResponse) {
        if(userResponse.equalsIgnoreCase(responses.get(TrueResponse)))
            this.point +=1;
    }

}
