package com.example.projetkhourchi;
import android.content.Context;
import android.content.res.Resources;
import android.icu.text.Normalizer2;
import android.util.Log;
import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class QCM implements Serializable {
    private ArrayList<Questionnaire> questionnaires;
    private static final String DATAFOLDER = "data";

    public QCM(){
        questionnaires = new ArrayList<>();
    }

    // get the list of questionnaires
    public ArrayList<Questionnaire> getQuestionnaires() {
        return questionnaires;
    }

    // get the list of categories

    public ArrayList<String> getCategories(){
        ArrayList<String> categories = new ArrayList<>();
        for(Questionnaire q: questionnaires){
            categories.add(q.getCategory());
        }
        return categories;
    }

    // ajouter un questionnaire

    public void addQuestionnaire(Questionnaire questionnaire){
        questionnaires.add(questionnaire);
    }

    // get the questionnaire by category

    public Questionnaire getQuestionnaireByCategory(String category){
        for(Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getCategory().equalsIgnoreCase(category))
                return questionnaire;
        }
        return null;
    }

    // check if the questionnaire exist

    public int isExist(String category){
        for(Questionnaire q: questionnaires)
            if(q.getCategory().equalsIgnoreCase(category))
                return questionnaires.indexOf(q);
        return -1;
    }

    // load the questionnaire from the file

    public void loadQuestionnaire(Context context, Resources resources) throws Exception {
        Field[] fields = R.raw.class.getFields();
        for (Field field : fields) {
                Questionnaire questionnaire = getQuestionnaireFromFile(resources, field.getInt(null));
                addQuestionnaire(questionnaire);
            }

        System.out.println("start admin file");
        File folder = new File(context.getFilesDir(), DATAFOLDER);
        File[] files = folder.listFiles();
        if(files!=null){
            for(File file: files){
                String filename = file.getName();
                if(!filename.contains("qcm")) continue;
                Log.d("file name", filename);
                Questionnaire questionnaire = getQuestionnaireFromFileAdmin(file);
                addQuestionnaire(questionnaire);
            }
        }
        readScoreInfile(context);
    }
    // read the score from the file

    private void readScoreInfile(Context context) throws IOException {
        File folder = new File(context.getFilesDir(), DATAFOLDER);
        File[] files = folder.listFiles();
        if(files!=null){
            for(File file: files){
                String filename = file.getName();
                if(filename.contains("score")){
                    InputStream inputStream = new FileInputStream(file);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] data = line.split(";");
                        String category = data[0];
                        float score =Float.parseFloat(data[1]);
                            int index = isExist(category);
                            if(index!=-1){
                                questionnaires.get(index).setScore(score);
                            }
                    }
                }
            }
        }
    }

    // save the score in the file

    public void saveScoreInFile(Context context) throws IOException {
        File folder = new File(context.getFilesDir(), DATAFOLDER);
        if(!folder.exists()){
            folder.mkdir();
        }
        File file = new File(folder, "score.txt");
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        for(Questionnaire questionnaire: questionnaires){
            osw.write(questionnaire.getCategory()+";"+questionnaire.getScore()+"\n");
        }
        osw.close();
        fos.close();
    }

    // calculate the average score

    public float calculMoyenneQCM(){
        float totalPoint = 0;
        for(Questionnaire q: questionnaires){
            totalPoint += (float)q.getScore()*20/(q.size());
        }
        return totalPoint / 20;
    }

    // register the questionnaire in the file

    public static void RegisterQuestionnaireInFile(Context context, Questionnaire questionnaire) throws IOException {
        File folder = new File(context.getFilesDir(), DATAFOLDER);
        if (!folder.exists()) {
            folder.mkdir(); // Crée le dossier s'il n'existe pas
        }

        File file = new File(folder, String.format("qcm_%s.txt",questionnaire.getCategory()));
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        osw.write(questionnaire.getCategory()+"\n");
        for (Question question : questionnaire.getListQuestions()) {
            osw.write("\n"+question.getQuestion() + "\n\n");
            for(String response : question.getResponses()) {
                if(question.getResponses().indexOf(response)==question.getTrueResponse())
                    osw.write("\t"+response+" x\n");
                else
                    osw.write("\t"+response+"\n");
            }

        }
        osw.close();
        fos.close();
    }


    // get the questionnaire from the file

    public Questionnaire getQuestionnaireFromFile(Resources res,  int filePath) throws IOException {
        InputStream inputStream = res.openRawResource(filePath);
        InputStreamReader isr = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(isr);
        String category = br.readLine();
        ArrayList<Question> listQuestions = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            int indexTrueResponse = -1;
            if (!line.trim().isEmpty()) {
                String nameQuestion = line.trim();
                do {
                    line = br.readLine();
                } while (line.trim().isEmpty());
                ArrayList<String> optionsResponses = new ArrayList<>();
                for (int i = 0; i < 3; i++) {
                    if (line.trim().endsWith("x")) {
                        optionsResponses.add(line.trim().substring(0, line.trim().length() - 1));
                        indexTrueResponse = i;
                    } else {
                        optionsResponses.add(line.trim());
                    }
                    line = br.readLine();
                }
                listQuestions.add(new Question(nameQuestion, optionsResponses, indexTrueResponse));
            }
        }
        br.close();
        return new Questionnaire(category, listQuestions);
    }

    private Questionnaire getQuestionnaireFromFileAdmin(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String category = br.readLine();
        ArrayList<Question> listQuestions = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            int indexTrueResponse = -1;
            if (!line.trim().isEmpty()) {
                String nameQuestion = line.trim();
                do {
                    line = br.readLine();
                } while (line.trim().isEmpty());
                ArrayList<String> optionsResponses = new ArrayList<>();
                for (int i = 0; i < 3; i++) {
                    if (line.trim().endsWith("x")) {
                        optionsResponses.add(line.trim().substring(0, line.trim().length() - 1));
                        indexTrueResponse = i;
                    } else {
                        optionsResponses.add(line.trim());
                    }
                    line = br.readLine();
                }
                listQuestions.add(new Question(nameQuestion, optionsResponses, indexTrueResponse));
            }
        }
        br.close();
        return new Questionnaire(category, listQuestions);
    }




    public void resetScore() {
        questionnaires.forEach(questionnaire -> {
            if(!questionnaire.isFinish()){
                questionnaire.setScore(0);
            }
        });
    }

}