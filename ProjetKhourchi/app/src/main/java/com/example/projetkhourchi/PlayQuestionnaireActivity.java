package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class PlayQuestionnaireActivity extends AppCompatActivity {
    private TextView Category, ViewQuestion, StatusQst;
    private Button Stop, Valid;

    private RadioGroup radioGroupResponse;
    private Question currentQuestion;
    private int QuestionNumber;
    private int nbOfQuestions;
    private Questionnaire questionnaire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        Category = findViewById(R.id.Category);
        ViewQuestion = findViewById(R.id.ViewQuestion);
        StatusQst = findViewById(R.id.StatusQst);
        Stop = findViewById(R.id.Arreter);
        Valid = findViewById(R.id.valider);
        radioGroupResponse = findViewById(R.id.radioGroupResponse);
        questionnaire = getIntent().getSerializableExtra("key", Questionnaire.class);
        if (questionnaire != null) {
            Category.setText(questionnaire.getCategory());
            nbOfQuestions = (int) questionnaire.size();
            QuestionNumber += 1;
            if(nbOfQuestions < QuestionNumber) return;
            StatusQst.setText(String.format("Question %s / %s", QuestionNumber, nbOfQuestions));
            // RANDOME
            currentQuestion = questionnaire.getQuestion(QuestionNumber);
            ViewQuestion.setText(currentQuestion.getQuestion());
            ArrayList<String> responses = currentQuestion.getResponses();
            radioGroupResponse.removeAllViews();
            for (int i = 0; i < responses.size(); i++) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setId(View.generateViewId());
                radioButton.setText(responses.get(i));
                radioGroupResponse.addView(radioButton);
            }
        }

        Stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent(PlayQuestionnaireActivity.this, SelectQuestionnaireActivity.class));
                finish();
            }
        });

        Valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = findViewById(radioGroupResponse.getCheckedRadioButtonId());
                if(radioButton != null){
                    currentQuestion.checkResponseUser(radioButton.getText().toString());
                        if(QuestionNumber < nbOfQuestions) {
                            QuestionNumber += 1;
                            StatusQst.setText(String.format("Question %s / %s", QuestionNumber, nbOfQuestions));
                            currentQuestion = questionnaire.getQuestion(QuestionNumber);
                            ViewQuestion.setText(currentQuestion.getQuestion());
                            ArrayList<String> responses = currentQuestion.getResponses();
                            radioGroupResponse.removeAllViews();
                            for (int i = 0; i < responses.size(); i++) {
                                RadioButton radioButton1 = new RadioButton(PlayQuestionnaireActivity.this);
                                radioButton1.setId(View.generateViewId());
                                radioButton1.setText(responses.get(i));
                                radioGroupResponse.addView(radioButton1);
                            }
                        }else{
                            Toast.makeText(PlayQuestionnaireActivity.this, "Vous avez fini le questionnaire", Toast.LENGTH_SHORT).show();
                            questionnaire.calculScoreQuestionnaire();
                            questionnaire.setFinish(true);
                            Intent intent = new Intent(PlayQuestionnaireActivity.this, SelectQuestionnaireActivity.class);
                            intent.putExtra("keyQuest", questionnaire);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                }else{
                    Toast.makeText(PlayQuestionnaireActivity.this, "Veuillez choisir une réponse", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
