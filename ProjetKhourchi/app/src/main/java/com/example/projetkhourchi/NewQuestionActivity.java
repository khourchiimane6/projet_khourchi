package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class NewQuestionActivity extends AppCompatActivity{

    private EditText edtQuestion, edtResponse1, edtResponse2, edtResponse3;
    private Button valider, annuler;
    private RadioGroup RadioGroup;
    private RadioButton radioButton;
    private ArrayList<Question> Questions;

    private QCM qcml;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createquestion);
        TextView txtViewCategory = findViewById(R.id.txtViewCategoryQuestion);
        Intent data = getIntent();
        txtViewCategory.setText(data.getStringExtra("category"));
        edtQuestion = findViewById(R.id.edtQuestion);
        edtResponse1 = findViewById(R.id.edtResponse1);
        edtResponse2 = findViewById(R.id.edtResponse2);
        edtResponse3 = findViewById(R.id.edtResponse3);
        valider = findViewById(R.id.validerqst);
        annuler = findViewById(R.id.annulerqst);
        RadioGroup = findViewById(R.id.radioGroupResponse);
        for (int i = 0; i < 3; i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(String.valueOf(i+1));
            radioButton.setId(View.generateViewId());
            RadioGroup.addView(radioButton);
        }
        Questions = new ArrayList<>();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtQuestion.getText().toString().equals("") || edtResponse1.getText().toString().equals("") || edtResponse2.getText().toString().equals("") || edtResponse3.getText().toString().equals("")){
                    Toast.makeText(NewQuestionActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                }else{
                    if(RadioGroup.getCheckedRadioButtonId() == -1){
                        Toast.makeText(NewQuestionActivity.this, "Veuillez choisir une réponse", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int selectedId = RadioGroup.getCheckedRadioButtonId();
                    radioButton = findViewById(selectedId);
                    Question q =
                            new Question(
                                    edtQuestion.getText().toString(),
                                    new ArrayList<>(Arrays.asList(
                                            edtResponse1.getText().toString(),
                                            edtResponse2.getText().toString(),
                                            edtResponse3.getText().toString())), Integer.parseInt(radioButton.getText().toString())-1);

                    // afficher dans log
                    Questions.add(q);
                    edtQuestion.getText().clear();
                    edtResponse1.getText().clear();
                    edtResponse2.getText().clear();
                    edtResponse3.getText().clear();
                    RadioGroup.clearCheck();
                    Toast.makeText(NewQuestionActivity.this, "Question ajoutée", Toast.LENGTH_SHORT).show();
                }
            }
        });

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!Questions.isEmpty()){
                        QCM.RegisterQuestionnaireInFile(getApplicationContext(), new Questionnaire(txtViewCategory.getText().toString(), Questions));
                        startActivity(new Intent(NewQuestionActivity.this, SelectQuestionnaireActivity.class));
                    }
                } catch (IOException e) {
                    Toast.makeText(NewQuestionActivity.this, "Erreur d'enregistrement du qestionnaire", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



}
