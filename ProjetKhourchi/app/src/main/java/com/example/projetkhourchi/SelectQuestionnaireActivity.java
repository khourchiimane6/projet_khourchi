package com.example.projetkhourchi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public class SelectQuestionnaireActivity extends AppCompatActivity {

    private Button Annuler, Play;

    private QCM qcml;
    private ListView lv;
    private ArrayAdapter<String> adapter;
    private ActivityResultLauncher<Intent> launcher;
    int nbChecked;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectquestionnaire);

        Annuler = findViewById(R.id.annuler);
        Play = findViewById(R.id.play);
        lv = findViewById(R.id.listQst);
        Intent data = getIntent();
        qcml = data.getSerializableExtra("keyQCM", QCM.class);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, qcml.getCategories());
        lv.setAdapter(adapter);

        getResultIntent();
        Annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent(SelectQuestionnaireActivity.this, MainActivity.class));
                finish();
            }
        });

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = lv.getCheckedItemPositions();
                nbChecked = 0;
                for (int i = 0; i < lv.getCount(); i++) {
                    if (checked.get(i)) {
                        nbChecked++;
                        Intent intent = new Intent(SelectQuestionnaireActivity.this, PlayQuestionnaireActivity.class);
                        Questionnaire q = qcml.getQuestionnaireByCategory(adapter.getItem(i));
                        intent.putExtra("key", q);
                        launcher.launch(intent);
                        return;
                    }
                }
                lv.clearChoices();
                Toast.makeText(SelectQuestionnaireActivity.this, "Veuillez sélectionner un questionnaire", Toast.LENGTH_SHORT).show();
            }
        });

        Annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent(SelectQuestionnaireActivity.this, MainActivity.class);
                data.putExtra("keyQCM", qcml);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private void getResultIntent() {
        launcher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        switch (result.getResultCode()) {
                            case RESULT_OK:
                                Intent data = result.getData();
                                Questionnaire played = data.getSerializableExtra("keyQuest", Questionnaire.class);
                                int index = qcml.isExist(played.getCategory());
                                if(index!=-1){
                                    qcml.getQuestionnaires().set(index, played);
                                    Intent intent = new Intent(SelectQuestionnaireActivity.this, DisplayScoreActivity.class);
                                    intent.putExtra("keyQCM", qcml);
                                    launcher.launch(intent);
                                }
                                break;
                            case RESULT_CANCELED:
                                break;
                        }
                    }
                }
        );

    }

}
